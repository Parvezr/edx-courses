<h3>Create a Watson Studio Account</h3>
<p>If you don't have a Watson Studio account, follow these steps. If you do have an account, skip to the next section to log in and create a project.</p>
<ol>
<li>
<p>Go to <a href="https://dataplatform.cloud.ibm.com/?cm_mmc=Email_Newsletter-_-Developer_Ed+Tech-_-WW_WW-_edx-Cognitive-Class-DS0720EN-Watson-Studio-login-&amp;cm_mmca1=000026UJ&amp;cm_mmca2=10006555&amp;cm_mmca3=M12345678&amp;cvosrc=email.Newsletter.M12345678&amp;cvo_campaign=000026UJ" target="_blank">Watson Studio</a> and create an account by clicking <strong>Try it for Free</strong>.</p>
</li>
<li>
<p>Select a region from the drop-down menu.</p>
</li>
<li>
<p>Enter your email address and review the terms and conditions. Then, click <strong>Next</strong>. (If you have an IBM Cloud account, click <strong>Log in to activate Watson</strong>.)</p>
<p><img src="/static/DS0720EN_studio_account.png" alt="" type="saveimage" target="[object Object]" preventdefault="function(){r.isDefaultPrevented=n}" stoppropagation="function(){r.isPropagationStopped=n}" stopimmediatepropagation="function(){r.isImmediatePropagationStopped=n}" isdefaultprevented="function t(){return!1}" ispropagationstopped="function t(){return!1}" isimmediatepropagationstopped="function t(){return!1}" width="805" height="527" /></p>
</li>
</ol>
<img src='https://courses.edx.org/assets/courseware/v1/9910106325922a842e6062c95c7b10d3/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_studio_account.png'></img>
