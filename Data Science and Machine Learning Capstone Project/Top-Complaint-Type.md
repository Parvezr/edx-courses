<h3><span class="xblock-display-name">What Is the Top Complaint Type?<br /></span></h3>
<p>The goal of this exercise is to find the answer to the Question 1 of the problem statement:</p>
<p><strong>Which type of complaint should the Department of Housing Preservation and Development of New York City focus on first?</strong></p>
<p>In this exercise, you need to read back the 311 datasets that you stored in Cloud Object Store and explore the dataset.</p>
<p>By the end of this exercise, you need to figure out the correct Complaint Type that the Department of Housing Preservation and Development of New York City should focus on.</p>
<p>Add the answer to this question along with code and comments in a notebook. Upload the notebook in the subsection called "Question 1 - Which type of complaints The Department of Housing Preservation and Development of New York City should focus first?"&nbsp; in the section "Submit your work and Review your Peer's work' in module 'Submit Your Work and Grade Your Peers".</p>
<p></p>
