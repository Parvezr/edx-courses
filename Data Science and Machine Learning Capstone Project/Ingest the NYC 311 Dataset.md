### Ingest the NYC 311 Dataset

1. For your convenience, I have already downloaded the data and placed on an IBM server. You can download the data using this link: https://cocl.us/311_NYC_Dataset. The downloaded data will have complaints submitted between **2010** and **2020**.


- Use SODA URL of NYC311 dataset to ingest the data only for the Department of Housing Preservation and Development, which is identified as HPD in Agency column.

- Specify the total number of rows as 10 million so that you can download all the complaint data related to HPD.

- Download data only for relevant columns so that the data volume is manageable. These are the recommended 

fields: created_date,unique_key,complaint_type, incident_zip,incident_address,street_name,address_type,city,resolution_description, borough,latitude,longitude,closed_date,location_type,'status

The HTTP URL for the API call should look like this:

https://data.cityofnewyork.us/resource/fhrw-4uyv.csv?$limit=100000000&Agency=HPD&$select=created_date,unique_key,complaint_type,incident_zip,incident_address,street_name,address_type,city,resolution_description,borough,latitude,longitude,closed_date,location_type,status

- **Recommendation**: Parse the date fields by using the parse_dates option of Panda's read_csv function


- After the data is downloaded, upload it to IBM Cloud Object Store using appropriate credentials.