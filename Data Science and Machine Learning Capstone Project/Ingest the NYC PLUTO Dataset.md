### Ingest the NYC PLUTO Dataset

1. Use the PLUTO dataset URL to download the zip file: https://www1.nyc.gov/assets/planning/download/zip/data-maps/open-data/nyc_pluto_18v1.zip.

2. Extract the file. You should have five CSV files for the five New York city boroughs: Bronx, Brooklyn, Manhattan, Queens, and Staten Island.

3. Read each CSV file and upload them to the IBM Cloud Object Store by using your credentials.