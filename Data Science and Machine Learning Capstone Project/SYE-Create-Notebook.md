<h3>Create a Notebook in a Project</h3>
<p>After you set up a project and configured the environment, you need to create a notebook file, copy a sample notebook from the Community, or add a notebook from a catalog. You must have the Admin or Editor role in the project to create a notebook.</p>
<p>You can watch the video&nbsp;<a href="https://youtu.be/OYoiAnYU25E" target="_blank">Create a project for Jupyter Notebooks</a> to help you complete this task.</p>
<ol>
<li>
<p>Click <strong>Add to project</strong> from the top bar and then click <strong>NOTEBOOK</strong>.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/28e4e74e1f26cce027648a5cbbc1ff71/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_create_notebook1.png" alt="" width="544" height="515" /></p>
</li>
<li>
<p>On the New Notebook page, specify the method to create your notebook. You can create a blank notebook, upload a notebook file from your file system, or upload a notebook file from a URL. The notebook that you create or select must be a .pynb file.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/cd77bce8282af748b89379bc21a8d354/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_create_notebook2.png" alt="" width="271" height="112" /></p>
<p>To create a blank notebook:</p>
<ul>
<li>
<p>On the New notebook page, add a name and optional description for the notebook.</p>
</li>
<li>
<p>Specify the language as Python.</p>
</li>
<li>
<p>Specify the runtime environment. After you create an environment definition on the Environments page of your project,&nbsp; you can see the list of runtimes that you can select from when you create the notebook.</p>
</li>
<li>
<p>Click <strong>Create Notebook</strong>.</p>
<p>To get familiar with the structure of a notebook, see <a href="https://dataplatform.cloud.ibm.com/docs/content/analyze-data/parts-of-a-notebook.html" target="_blank">Parts of a notebook</a>.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/d185aaee619ce91ae381a8981ecda880/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_create_notebook3.png" alt="" width="628" height="725" /></p>
</li>
</ul>
<p>To add a notebook from your file system:</p>
<ul>
<li>
<p>On the New notebook page, click <strong>Browse</strong>.</p>
</li>
<li>
<p>Add an optional description for the notebook.</p>
<p>Specify the runtime environment. After you create an environment definition on the Environments page of your project,&nbsp; you can see the list of runtimes that you can select from when you create the notebook.</p>
</li>
<li>
<p>Click <strong>Create Notebook</strong>. The notebook opens in edit mode.</p>
<p><img src="https://courses.edx.org/assets/courseware/v1/28c2ce407c19873a4bfd61fb695afc60/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_create_notebook4.png" alt="" width="630" height="742" /></p>
</li>
</ul>
</li>
</ol>


```

```
