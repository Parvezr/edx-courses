<h1>Move Data to and from Cloud Object Storage</h1>
<h3>Create Credential and Bucket Variables</h3>
<ol>
<li>
<p>In a notebook, create a cell where you copy the credentials in Python variables as follows (in a hidden cell). Get the values for your credentials to replace the placeholder <code>****</code> strings from the credential information that you got in the previous section in the course when you created a Panda Dataframe from the uploaded file.</p>
<pre>client_cred = ibm_boto3.client(service_name='s3',
ibm_api_key_id='***',
ibm_auth_endpoint='***',
config=Config(signature_version='oauth'),
endpoint_url='***')
</pre>
</li>
<li>
<p>Create a bucket variable. Replace the <code>****</code> string with the bucket string that you got when you created a Panda Dataframe from the uploaded file in the previous section<strong> </strong></p>
<pre>bucket = '***'</pre>
<p><img src="https://courses.edx.org/assets/courseware/v1/cd4c16c0b824500da2fb59f4c88750c0/asset-v1:IBM+DS0720EN+1T2020+type@asset+block/DS0720EN_create_creds.png" alt="" width="929" height="620" /></p>
</li>
</ol>

<h3>Upload a File to Cloud Object Store by Using the Credential and Bucket Variables</h3>
<ol>
<li>
<p>Create a pickle (PKL) file out of the Dataframe:</p>
<pre>df.to_pickle('./df_raw.pkl')</pre>
</li>
<li>
<p>Upload the pickle (PKL) file:</p>
<pre>client_cred.upload_file('./df_raw.pkl',bucket,'df_raw_cos.pkl')</pre>
</li>
</ol>

<h3>Download a File from Cloud Object Store by Using Credential and Bucket Variables</h3>
<ol>
<li>
<p>Download the file from Cloud Object Store:</p>
<pre>client_cred.download_file(Bucket=bucket,Key='df_raw_cos.pkl',Filename='./df_raw_local.pkl')
</pre>
</li>
<li>
<p>Create a Dataframe out of the file:</p>
<pre>df = pd.read_pickle('./df_raw_local.pkl')</pre>
</li>
</ol>


```

```
