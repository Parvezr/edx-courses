<h3>Predict Complaint Types</h3>
<p>The goal of this exercise is to do Model Development and Validation to find the answer to the Question 4 of the problem statement:</p>
<p><strong>Can a predictive model be built for future prediction of the possibility of complaints of the specific type that you identified in response to Question 1?</strong></p>
<p>In this exercise, you will use a feature-engineered dataset to determine whether a predictive model can be built to predict the complaint (of the Complaint Type that you decided to focus on in Week 2) by using past data.</p>
<p>Using the best model, you need to predict the number of future complaints (of the Complaint Type that you decided to focus on in Question 1).</p>
<p>Add your answer to this question along with code and comments in a separate notebook. Upload the notebook in the subsection called "Question 4 - Can a predictive model be built for future prediction of the possibility of Complaints of the specific type that you identified in response to Question 1?" in the section "Submit your work and Review your Peer's work" in the module "Submit Your Work and Grade Your Peers".</p>
