## Lab Instructions

This course uses **Skills Network Labs**, an online virtual lab environment to help you get hands-on experience without the hassle of installing and configuring the tools. You will get access to popular open-source data science tools like **Jupyter Notebooks**, which you will use to get hands-on practice with Spark in this lab.

### Practice your skills on Skills Network Labs (External resource)

Skills Network Labs (SN Labs) is a virtual lab environment used in this course. Your Username and email will be passed to SN Labs and will only be used for communicating important information to enhance your learning experience.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DV0101EN/DV0101EN-Exercise-Generating-Maps-in-Python-py.ipynb?lti=true) 

In case, you encounter any issue in launching  Skills Network Labs or want to view the notebook in your own Jupyter environment, you may click on the link below

https://cocl.us/DV0101EN_edX_Notebook5
