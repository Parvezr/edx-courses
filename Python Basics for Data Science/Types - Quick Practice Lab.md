As we said before, *Jupyter* notebooks are a tool of choice for data scientists and all of the hands-on labs in this course are provided as Jupyter notebooks that you will run in *JupyterLab*, the next generation Jupyter notebook environment.

This lab, once again, will be using *Skills Network Labs*, a virtual lab environment that has everything you will need to do your hands on exercises. You do not have to install anything on our computer. Your folders with data and notebooks are all hosted for you and you can come and use the environment any time from anywhere. The only thing you need is a recent version of Chrome or Firefox (sorry but we do not support Safari at this time) browser on a laptop or a desktop computer (no phones or tablets).

If you are not familiar with JupyterLab and Jupyter notebooks, there are lots of in-depth tutorials you can find on the web. Just skip the sections that talk about installing JupyterLab. We don't recommend spending too much time on learning JupyterLab at this point; this is something you should do later. This [introductory video](https://youtu.be/A5YyoCKxEOU) may be everything you need for now. 

### Types in Python (External resource)

Skills Network Labs (SN Labs) is a virtual lab environment used in this course. Your Username and email will be passed to SN Labs and will only be used for communicating important information to enhance your learning experience.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/PY0101EN/PY0101EN-1.2_notebook_quizz_types.ipynb?lti=true) 
