In this Lab you will load a customer dataset related to a telecommunication company, clean it, use KNN (K-Nearest Neighbours to predict the category of customers, and evaluate the accuracy of your model.


Let's learn about KNN and see how we can apply it real world problems.


**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: KNN (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Clas-K-Nearest-neighbors-CustCat-py-v1.ipynb?lti=true) 

You can download the lab [HERE](https://cocl.us/ML0101EN-Clas-K-Nearest-neighbors-CustCat-py-v1.ipynb)
