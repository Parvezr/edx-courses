In this lab, we learn how to use scikit-learn library to implement Multiple linear regression. We again use the Carbon dioxide emission dataset to build a model, Evaluate the model, and finally use model to predict unknown value. Lets get started.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: Multiple Linear Regression (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Reg-Mulitple-Linear-Regression-Co2-py-v1.ipynb?lti=true) 

You can download the lab [HERE](https://cocl.us/ML0101EN-Reg-Mulitple-Linear-Regression-Co2-py-v1.ipynb)