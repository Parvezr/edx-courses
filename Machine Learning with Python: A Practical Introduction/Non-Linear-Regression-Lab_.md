If the data shows a curvy trend, then linear regression will not produce very accurate results when compared to a non-linear regression because, as the name implies, linear regression presumes that the data is linear.

Let's learn about non linear regressions and apply an example on python. In this notebook, we fit a non-linear model to the data points corresponding to China's GDP from 1960 to 2014.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: Non-linear Regression (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Reg-NoneLinearRegression-py-v1.ipynb?lti=true) 

You can download the lab [HERE](https://cocl.us/ML0101EN-Reg-NoneLinearRegression-py-v1.ipynb)