In this lab, you learn about Polynomial regressions and apply an example on python. You learn how to transfer your feature sets, and then use Multiple Linear Regression, to solve such a problem.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: Polynomial Regression (External resource)
Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/edX/ML0101EN/ML0101EN-Reg-Polynomial-Regression-Co2-py-v1.ipynb?lti=true)

You can download the lab [HERE](https://cocl.us/ML0101EN-Reg-Polynomial-Regression-Co2-py-v1.ipynb)