In this notebook, you will use SVM (Support Vector Machines) to build and train a model using human cell records, and classify cells to whether the samples are benign or malignant.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: SVM (Support Vector Machines) (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/ML0101EN/ML0101EN-Clas-SVM-cancer-py-v1.ipynb?lti=true)

You can download the lab [HERE](https://cocl.us/ML0101EN-Clas-SVM-cancer-py-v1.ipynb)