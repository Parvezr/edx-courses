In this notebook, you will learn Logistic Regression, and then, you'll create a model with telecommunications data to predict when its customers will leave for a competitor, so that you can take some action to retain the customer.

**Please note that the practice labs (except the last week assignment) are optional and are provided for you to practice and understand the topic. Therefore, you do not need to submit those, as they are not graded, and won't be updated as complete. Just run the codes to see the results, and feel free to change it.**

### Lab: Logistic Regression (External resource)

Start the lab by clicking the button below. Your username and password will be sent to Skills Network Labs.

[View resource in a new window](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/ML0101EN/ML0101EN-Clas-Logistic-Reg-churn-py-v1.ipynb?lti=true)

You can download the lab [HERE](https://cocl.us/ML0101EN-Clas-Logistic-Reg-churn-py-v1.ipynb)