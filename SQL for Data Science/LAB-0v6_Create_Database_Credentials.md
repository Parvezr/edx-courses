## Create credentials to access your database instance

Database credentials are required to connect from remote applications like Jupyter notebooks which are used in the labs and assignment in the last two weeks of the course.

0. Go to your IBM Cloud Resources dashboard (or click on IBM Cloud in the top left corner):

https://cloud.ibm.com/resources

Note: you may need to log into IBM Cloud in the process of loading the resources/dashboard.

If your connection is slow it may take over 30 seconds for the dashboard to fully load.

1. Locate and click on your Db2 service listed under Services.

(**NOTE:** In the example below the service is called “Db2-xx” but your Db2 service may have a
different letters/numbers in the suffix e.g. “Db2-f8”, “Db-50”, etc.)

![image](images/ServicesDb2xx.PNG)

2. Click on Service Credentials in the left menu

![image](images/Service_cred.PNG)

3. Click on the button to create **New credential**

![image](images/New_Cred.PNG)

In the prompt that comes up click the “Add” button in the bottom right:

![image](images/Add_Cred_1.PNG)

4. Check the box to **View credentials**
5. Copy and save the credentials making a note of the following:

- **port** is the database port
- **db** is the database name
- **host** is the hostname of the database instance
- **username** is the username you'll use to connect
- **password** is the password you'll use to connect
- (you may need to scroll down to see the password)
- **URI** (you will need this for Jupyter notebooks when using SQL Magic)

![image](images/cred_details.PNG)