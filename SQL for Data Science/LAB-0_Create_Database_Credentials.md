## LAB 0: Create Database Credentials (5:00)

Follow the instructions in the attached document to create Service Credentials for your Db2 instance.

**LAB Instructions:** [Lab 0 - v6 - Create Db2 Service Credentials](./LAB-0v6_Create_Database_Credentials.md)