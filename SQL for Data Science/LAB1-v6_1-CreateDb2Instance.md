## LAB 1: Create your IBM Cloud account and Provision your Database instance

The hands-on labs for this course require an environment for working with a relational database. To get you up and running quickly we will do so on the Cloud, so you don't have to worry about downloading or installing anything, rather, simply access your database from your web browser. IBM Cloud provides a large number of Data and Analytics services, including IBM Db2, a next generation SQL database. And best of all you can create a Lite account on IBM Cloud and provision an instance of Db2 (Lite Plan) at no charge, and without providing any credit card information.

You will complete this Lab in two parts:
1. Create an IBM Cloud Lite account
2. Provision an instance of IBM Db2 Lite plan

## Part I: Create an IBM Cloud Lite account

First let us introduce you to IBM Cloud. IBM Cloud is IBM's innovative cloud computing
platform which combines platform as a service (PaaS) with infrastructure as a service (IaaS). It
includes a rich catalog of cloud services that can be easily integrated with PaaS and IaaS to build
business applications rapidly. It lets you quickly create, deploy, and manage your applications in
the cloud. You don't have to deal with any of the underlying infrastructure. And you don't have
to deal with the hosting and the managing of your cloud-based applications. You can use the
tools and languages of your choice. IBM Cloud comes with a catalog full of awesome services,
or APIs, from IBM and from third parties that you can use in your applications. You can scale
your applications with literally a couple of clicks.

Please follow the steps given below to create an IBM Cloud account.
1. Sign up for IBM Cloud at: https://cocl.us/ibmcloud-edx-sql

Note: If you already have an IBM Cloud account (perhaps you created it for another course), you
can skip this step and simply log in. If your older IBM Cloud account trial has expired and is
asking for a credit card to upgrade, you can instead create a new IBM Cloud account with a
different email address.

![image](images/CreateAccount_M1.PNG)

2. As part of the sign-up process you will receive an email in your inbox. You will need to click
on the link in this email to **verify your email** and activate your account.

3. Log-in to IBM Cloud by going to the link in step 1.

## Part II: Provision an instance of IBM Db2 Lite plan

Now let us introduce you to Db2 on Cloud. IBM Db2 is a next generation SQL database
provisioned for you in the cloud. You can use Db2 on Cloud just as you would use any database
software (RDBMS), but without the overhead and expense of hardware setup or software
installation and maintenance. Among the service plans offered for Db2 on IBM Cloud is the Lite
plan, which is free to use. You can use your database instance to store relational data, analyze
data using built-in SQL editor, or by connecting your own apps. 

Note that IBM Cloud also provides other variants of Db2 such as Db2 Hosted and Db2
Warehouse on Cloud, which is also referred to in this course. However for the labs in this course
we will utilize the Db2 service since it comes with a Lite plan which is free to use.

Please follow the steps given below to provision an instance of a Db2 on IBM Cloud

1. Ensure you are logged in to IBM Cloud and go to: https://cocl.us/db2-edx-sql4ds

Note: Alternatively you can click on the **Catalog** menu item, scroll down to the section for
**Databases**, and click on the tile for **Db2** service.

![image](images/Catalog.jpg)

[Make sure you choose “Db2”. **Do NOT choose “Db2 Warehouse” or “Db2 Hosted” or “SQL
Query”**.]

![image](images/Db2.jpg)

Note: Depending on the Country of your IBM Cloud account a region/location to deploy will be
pre-selected. For example if you are in the US, the default Datacenter location will be Dallas.
Users from UK will see London and so on. It is best to go with the default location that is closest
to you.

![image](images/Db2Details.jpg)

2. Scroll down to Pricing Plans and select the Lite plan (it’s a free plan and does not require
credit card).

3. Then click on the Create button towards the bottom right of the page.

It will spin for a few seconds (typically less than 30s) and then you should see a Service Created
message indicating that your instance of Db2 was created successfully.

![image](images/Instance.jpg)
