# Hands-on LAB: Composing and Running Basic SQL Queries

## Lab: Composing and Running basic SQL queries

So far in this module you learned about the five basic SQL statements to create tables, insert data, select results, update, and delete data. In this lab, you will practice composing and running these statements via hands-on experiences.

To write and execute the SQL statements in this lab, you will need to launch the SQL editor (Run SQL) in your Db2 database instance on IBM Cloud that you created in the previous lab.

**NOTE**: Some users have reported issues performing this lab using Microsoft Edge. It is recommended using Google Chrome or Mozilla Firefox as your web browser to complete these labs.

First you will launch the SQL editor in the Db2 console using the following steps:

1) Go to your IBM Cloud dashboard (you may need to log into IBM Cloud in the process):

https://cloud.ibm.com/resources

2) Expand the Cloud Foundry Services and locate and click on your instance of Db2 you provisioned in the previous lab (the name typically starts with Db2-xx for example Db2-fk, Db2-50, etc.)

![image](images/ServicesDb2xx.PNG)

3) Click on the **Open Console** button.

![image](images/Db2xxOpenConsole.png)

4) The Db2 console will open in a new tab in your web browser. Click on the 3 bar menu icon in the top left corner and then click on RUN SQL.

![image](images/Run_SQL.jpg)

On the next screen click on "Blank"

![image](images/RunSQL-Blank.png)

5) The SQL editor will open where you can start typing and running queries.

![image](images/SQLeditor.png)

The SQL editor has several areas for different things:

![image](images/SQLEditorZones.png)

Now let’s practice creating and running some SQL queries in the SQL editor.

Consider the following table called **INSTRUCTOR:**

![image](images/Instructor.jpg)

**Task 0:** Drop the table INSTRUCTOR from the database in case it already exists, so that we start from a clean state.

(Hint: Ignore the undefined error if this table does not already exist in your database)

**Task 1:** Create the INSTRUCTOR table as defined above. Have the ins_id be the primary key, and ensure the lastname and firstname are not null.

(Hint: ins_id is of type INTEGER, country of type CHAR(2), and rest of the fields VARCHAR)

**Task 2A:** Insert one row into the INSTRUCTOR table for the the instructor Rav Ahuja.

(Hint: values for the character fields require a singe quotation mark (') before and after each value)

**Task 2B:** Insert two rows at once in the INSTRUCTOR table for instructors Raul Chong and Hima Vasudevan.

(Hint: list the values for the second row after the first row)

**Task 3:** Select all rows from the INSTRUCTOR table.

**Task 3B:** Select the firstname, lastname and country where the city is Toronto

**Task 4:** Update the row for Rav Ahuja and change his city to Markham.

**Task 5:** Delete the row for Raul Chong from the table.

**Task 5B:** Retrieve all rows in the INSTRUCTOR table

Solutions for Lab Tasks

Please try the above tasks on your own and only use the solutions if you need a hint or want to verify your queries.

**NOTE:** You can type multiple SQL queries in the SQL editor. Just type the query terminator character after each query. The default terminator is the semi-colon ;

Comments by default are preceded with a double dash --

So if you copy and paste all of the queries in the file below into the SQL editor and then **Run -> Run All**, all of the queries will be executed.

Here are the solutions:

[Lab2-solution-Script.sql.txt](./Lab2-solution-Script.sql)