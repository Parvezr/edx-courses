## Hands-on Lab: Built-in Functions (20:00)

In the previous lesson you gained an understanding of built-in database functions.

Now create and populate the PETSALE table in the database using the attached SQL script, and practice the queries in the previous lesson to solidify your understanding of built-in database functions.

[PETSALE-CREATE.sql](https://gitlab.com/simrangarg/edx-courses/-/blob/master/SQL%20for%20Data%20Science/PETSALE-CREATE.sql)

**Solutions:**

[PETSALE-FUNCTIONS.sql](https://gitlab.com/simrangarg/edx-courses/-/blob/master/SQL%20for%20Data%20Science/PETSALE-FUNCTIONS.sql)