## Hands-on LAB: String Patterns, Sorting & Grouping (1 Hr)

### LAB: String Patterns, Sorting & Grouping

To complete the Lab for this week, please refer to the instructions in the attached pdf document:

### Lab instructions:

[Lab3v5_Instructions](./Lab3v5.md)

### Download this script file to create tables:

[Script_Create_Tables .SQL | .TXT](./Script_Create_Tables.txt)

### Download the following data files (in case the .csv files gets downloaded with the .xls extenstion you can try downloading the .txt files instead by right clicking on the .TXT link and Save Link As...):

[Employees .CSV | .TXT](./Employees.txt)

[Departments .CSV |.TXT](./Department.txt)

[Jobs .CSV | .TXT](./Jobs.txt)

[Locations .CSV | .TXT](./Location.txt)

[job History .CSV | .TXT](./Job_History.txt)

### Solutions: Download the following script and text files:

[Lab3_Queries_v4.sql.txt](./Lab_Queries.txt)

[Lab3_Queries-v4.sql](./Lab3_Queries.sql)