### Lab: Data Wrangling (External resource)

By the end of this notebook, you will have learned the basics of Data Wrangling.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/data-wrangling.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Data-Wrangling.ipynb)

