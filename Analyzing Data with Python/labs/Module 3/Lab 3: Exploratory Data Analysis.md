### Lab 3: Exploratory Data Analysis (External resource)

In this section, we will explore several methods to see if certain characteristics or features can be used to predict price.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/exploratory-data-analysis.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Exploratory-Data-Analysis.ipynb)

