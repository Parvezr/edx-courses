### Lab: Model Evaluation (External resource)

We have built models and made predictions of vehicle prices. Now we will determine how accurate these predictions are.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/model-evaluation-and-refinement.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Model-Evaluation-and-Refinement.ipynb)

