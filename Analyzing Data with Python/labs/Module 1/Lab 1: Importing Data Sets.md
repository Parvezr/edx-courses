### Lab 1: Importing Data Sets (External resource)

In this lab, you will learn the basics of loading and viewing data with Pandas

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/DA0101EN/review-introduction.ipynb?lti=true) 


You can download the lab [HERE](https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/coursera/DA0101EN-Review-Introduction.ipynb)

